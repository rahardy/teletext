<!-- SPDX-FileCopyrightText: © 2021 Tech and Software Ltd. -->
<!-- SPDX-License-Identifier: AGPL-3.0-only OR LicenseRef-uk.ltd.TechAndSoftware-1.0 -->

These pages in testpages.json are from https://archive.teletextarchaeologist.org/ via Simon Rawles' editor at https://edit.tf :

* Engineering: BBC2, 2 March 1988, page 797 (tweaked to correct the spelling of 'White')
* Advert: Channel 4, 25 April 1992, page 617
* UK: BBC1, 25 December 1995, page 196

The data format is base64url-based, and described in the edit.tf editor docs: https://github.com/rawles/edit.tf
